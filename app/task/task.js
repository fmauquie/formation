'use strict';

angular.module('app')
  .config([
    '$routeProvider',
    function($routeProvider) {
      $routeProvider.when('/tasks/12', { // TODO paramétriser la route. On veut choisir l'ID de la tâche !
        templateUrl: 'task/task.html'
      });
    }
  ])
  .controller('TaskController', [
    // TODO injecter les paramètres de la route
    'Task',
    function(/* TODO injecter les paramètres de la route */ Task) {
      this.task = null; // TODO Récupérer la tâche en utilisant le paramètre
    }
  ])
;
